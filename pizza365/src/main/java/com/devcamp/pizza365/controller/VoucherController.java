package com.devcamp.pizza365.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.model.CVoucher;
import com.devcamp.pizza365.service.VoucherService;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class VoucherController {
  @Autowired
  VoucherService voucherService;

  @GetMapping("/vouchers")
  public ResponseEntity<List<CVoucher>> getAllVouchers() {
    try {
      return new ResponseEntity(voucherService.getAllVouchers(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
