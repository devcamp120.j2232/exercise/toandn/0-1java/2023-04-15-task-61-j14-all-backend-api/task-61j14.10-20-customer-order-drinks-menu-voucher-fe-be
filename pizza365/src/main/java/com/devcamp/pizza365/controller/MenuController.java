package com.devcamp.pizza365.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.model.CMenu;
import com.devcamp.pizza365.service.MenuService;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class MenuController {
  @Autowired
  MenuService menuService;

  @GetMapping("/combo-menu")
  public ResponseEntity<List<CMenu>> getComboMenu() {
    try {
      return new ResponseEntity(menuService.getComboMenu(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
