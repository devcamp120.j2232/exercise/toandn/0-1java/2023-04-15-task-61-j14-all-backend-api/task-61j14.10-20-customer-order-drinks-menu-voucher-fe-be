package com.devcamp.pizza365.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.model.Customer;
import com.devcamp.pizza365.model.Order;
import com.devcamp.pizza365.service.CustomerService;
import com.devcamp.pizza365.service.OrderService;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)

public class OrderController {
  @Autowired
  CustomerService customerService;

  @Autowired
  OrderService orderService;

  @GetMapping("/devcamp-list-orders")
  public ResponseEntity<List<Order>> getAllOrders() {
    try {

      return new ResponseEntity(orderService.getAllOrders(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/devcamp-orders")
  public ResponseEntity<Set<Order>> getOrderByCustomerId(@RequestParam(value = "customerId") Long customerId) {
    try {
      Set<Order> vOrder = customerService.getOrderByCustomerId(customerId);
      if (vOrder != null) {
        return new ResponseEntity<>(vOrder, HttpStatus.OK);
      } else {
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
      }
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
