package com.devcamp.pizza365.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "orders")
public class Order {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long order_id;
  @Column(name = "order_code")
  private String orderCode;
  @Column(name = "pizza_size")
  private String pizzaSize;
  @Column(name = "pizza_type")
  private String pizzaType;
  @Column(name = "voucher_code")
  private String voucherCode;
  @Column(name = "price")
  private Long price;
  @Column(name = "paid")
  private Long paid;
  @ManyToOne
  @JsonIgnore
  @JoinColumn(name = "customerId")
  private Customer customer;

  public Order() {
  }

  public Order(Long order_id, String orderCode, String pizzaSize, String pizzaType, String voucherCode, Long price,
      Long paid, Customer customer) {
    this.order_id = order_id;
    this.orderCode = orderCode;
    this.pizzaSize = pizzaSize;
    this.pizzaType = pizzaType;
    this.voucherCode = voucherCode;
    this.price = price;
    this.paid = paid;
    this.customer = customer;
  }

  public Long getOrder_id() {
    return order_id;
  }

  public void setOrder_id(Long order_id) {
    this.order_id = order_id;
  }

  public String getOrderCode() {
    return orderCode;
  }

  public void setOrderCode(String orderCode) {
    this.orderCode = orderCode;
  }

  public String getPizzaSize() {
    return pizzaSize;
  }

  public void setPizzaSize(String pizzaSize) {
    this.pizzaSize = pizzaSize;
  }

  public String getPizzaType() {
    return pizzaType;
  }

  public void setPizzaType(String pizzaType) {
    this.pizzaType = pizzaType;
  }

  public String getVoucherCode() {
    return voucherCode;
  }

  public void setVoucherCode(String voucherCode) {
    this.voucherCode = voucherCode;
  }

  public Long getPrice() {
    return price;
  }

  public void setPrice(Long price) {
    this.price = price;
  }

  public Long getPaid() {
    return paid;
  }

  public void setPaid(Long paid) {
    this.paid = paid;
  }

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

}
