package com.devcamp.pizza365.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365.model.Customer;

public interface ICustomerRepository extends JpaRepository<Customer, Long> {
  Customer findByCustomerId(Long customerId);
}
