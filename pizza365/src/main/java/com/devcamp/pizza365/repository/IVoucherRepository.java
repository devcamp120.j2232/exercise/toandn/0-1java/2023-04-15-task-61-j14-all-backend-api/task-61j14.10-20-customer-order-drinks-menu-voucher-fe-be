package com.devcamp.pizza365.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.model.CVoucher;

@Service
public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {

}