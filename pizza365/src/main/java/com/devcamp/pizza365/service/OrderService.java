package com.devcamp.pizza365.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.model.Order;
import com.devcamp.pizza365.repository.IOrderRepository;

@Service
public class OrderService {
  @Autowired
  IOrderRepository pIOrderRepository;

  public ArrayList<Order> getAllOrders() {
    ArrayList<Order> pOrders = new ArrayList<>();
    pIOrderRepository.findAll().forEach(pOrders::add);
    return pOrders;
  }
}