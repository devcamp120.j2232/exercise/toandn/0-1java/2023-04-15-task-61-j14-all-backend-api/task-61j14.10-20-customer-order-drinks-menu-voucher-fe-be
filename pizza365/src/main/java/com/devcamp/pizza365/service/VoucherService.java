package com.devcamp.pizza365.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.model.CVoucher;
import com.devcamp.pizza365.repository.IVoucherRepository;

@Service
public class VoucherService {
  @Autowired
  IVoucherRepository pIVoucherRepository;

  public ArrayList<CVoucher> getAllVouchers() {
    ArrayList<CVoucher> listVoucher = new ArrayList<>();
    pIVoucherRepository.findAll().forEach(listVoucher::add);
    return listVoucher;
  }
}
